FROM node:12.14.1-alpine3.9

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV production

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

ENV PORT 3000
EXPOSE $PORT
CMD [ "npm", "start" ]
